const httpStatus = require('http-status');
const passport = require('passport');
const User = require('../api/models/user.model');
const StatusError = require('../utilities/errors.utils');

const ADMIN = 'admin';
const LOGGED_USER = '_loggedUser';

const handleJWT = (req, res, next, roles) => async (err, user, info) => {
  const error = err || info;
  const logIn = Promise.promisify(req.logIn);
  const statusError = new StatusError({
    message: error ? error.message : 'Unauthorized',
    status: httpStatus.UNAUTHORIZED,
    stack: error ? error.stack : undefined,
  });

  try {
    if (error || !user) throw error;
    await logIn(user, { session: false });
  } catch (e) {
    return next(statusError);
  }

  if (roles === LOGGED_USER) {
    if (user.role !== 'admin' && req.params.userId !== user._id.toString()) {
      statusError.status = httpStatus.FORBIDDEN;
      statusError.message = 'Forbidden';
      return next(statusError);
    }
  } else if (!roles.includes(user.role)) {
    statusError.status = httpStatus.FORBIDDEN;
    statusError.message = 'Forbidden';
    return next(statusError);
  } else if (err || !user) {
    return next(statusError);
  }

  req.user = user;

  return next();
};

exports.ADMIN = ADMIN;
exports.LOGGED_USER = LOGGED_USER;

exports.authorize = (roles = User.roles) => (req, res, next) =>
  passport.authenticate(
    'jwt', { session: false },
    handleJWT(req, res, next, roles),
  )(req, res, next);

exports.oAuth = service =>
  passport.authenticate(service, { session: false });