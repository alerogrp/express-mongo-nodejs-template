const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route')

const router = express.Router();

/**
 * GET api/status
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET api/routes
 */
router.use('/users', userRoutes);
router.use('/auth', authRoutes);

module.exports = router;