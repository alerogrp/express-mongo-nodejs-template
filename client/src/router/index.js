import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import AdminDash from '@/views/admin/index'
import UserDash from '@/views/user/index'
import Login from '@/views/auth/Login'
import Register from '@/views/auth/Register'
import ForgotPassword from '@/views/auth/Forgot-password.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  },
  {
    path: '/dashboard',
    name: 'user',
    component: UserDash,
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/admin-dashboard',
    name: 'admin',
    component: AdminDash,
    meta: {
      requiresAuth: true,
      is_admin: true
    }
  },
  {
    path: '/auth/login',
    name: 'login',
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: '/auth/register',
    name: 'register',
    component: Register,
    meta: {
      guest: true
    }
  },
  {
    path: '/auth/recover-password',
    name: 'recoverpassword',
    component: ForgotPassword,
    meta: {
      guest: true
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
