import Vue from 'vue'
import App from './App.vue'
import router from './router'
import bootvue from './plugins/bootvue'

Vue.config.productionTip = false

new Vue({
  router,
  bootvue,
  render: h => h(App)
}).$mount('#app')
