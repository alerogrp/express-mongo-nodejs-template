import Vue from 'vue'
import BootstrapVue, { CardPlugin, FormPlugin, ImagePlugin, LayoutPlugin, InputGroupPlugin, DropdownPlugin, ButtonPlugin, LinkPlugin, NavPlugin, NavbarPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

// Import the individual components
// Add components globally
Vue.use(CardPlugin)
Vue.use(FormPlugin)
Vue.use(ImagePlugin)
Vue.use(LayoutPlugin)
Vue.use(InputGroupPlugin)
Vue.use(DropdownPlugin)
Vue.use(ButtonPlugin)
Vue.use(LinkPlugin)
Vue.use(NavPlugin)
Vue.use(NavbarPlugin)
